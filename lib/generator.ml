open Labyrinth
open Labyrinth.Syntax

type table = cell option array

let bit_of_cell = function
   | Wall -> 1
   | Room -> 0
let table_index_of_five_cells a b c d e =
        (bit_of_cell e lsl 0)
   lxor (bit_of_cell d lsl 1)
   lxor (bit_of_cell c lsl 2)
   lxor (bit_of_cell b lsl 3)
   lxor (bit_of_cell a lsl 4)

let generate_cell ~prng table a b c d e =
   match table.(table_index_of_five_cells a b c d e) with
   | Some cell -> cell
   | None -> Prng.cell prng

let init_cell ~prng table prev curr index =
   assert (0 <= index);
   let line_length = length prev in
   assert (index < line_length);
   if index = 0 then
      (* special case because of left-hand side edge *)
      let b = Prng.cell prng in
      generate_cell ~prng table Wall Room b prev.?(index) prev.?(index + 1)
   else if index = 1 then
      (* special case because of left-hand side edge *)
      generate_cell ~prng table Room curr.(index - 1) prev.?(index - 1) prev.?(index) prev.?(index + 1)
   else if index < line_length - 1 then
      (* normal case *)
      generate_cell ~prng table curr.(index - 2) curr.(index - 1) prev.?(index - 1) prev.?(index) prev.?(index + 1)
   else if index = line_length - 1 then
      (* special case because of right-hand side edge *)
      let r = Prng.cell prng in
      generate_cell ~prng table curr.(index - 2) curr.(index - 1) prev.?(index - 1) prev.?(index) r
   else
      assert false (* out-of-bounds *)

let generate_line ~prng table previous_line =
   let line_length = length previous_line in
   let line = Array.make line_length Room in
   for index = 0 to line_length - 1 do
      line.(index) <- init_cell ~prng table previous_line line index
   done;
   Labyrinth.line line

let generate ~prng table first_line number_of_lines =
   if (Array.length table <> 32) then raise (Invalid_argument "Generator.generate: size of table ≠ 32");
   if (number_of_lines < 1) then raise (Invalid_argument "Generator.generate: number_of_line < 1");
   let rec loop ~prng labyrinth number_of_lines =
      if number_of_lines = 0 then
         labyrinth
      else
         let line = generate_line ~prng table (List.hd labyrinth) in
         loop ~prng (line :: labyrinth) (number_of_lines - 1)
   in
   loop ~prng [ first_line ] (number_of_lines - 1)
