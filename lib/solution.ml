type cell = Labyrinth.cell = Wall | Room

let cost_of_cell = function Wall -> 1 | Room -> 0

type t = int array list

type direction =
   | Up (* when the solver is going up the labyrinth *)
   | Down (* when the solver is going down the labyrinth *)

(* When the solver goes up, it only considers the current line and the line
   below to determine the cost to reach a cell. That's because when going up,
   it's coming from the line below. Conversly, when going down, it considers
   neighbours from the line above. *)

let neighbours dir line col =
   let sides = [ (line, col - 1); (line, col + 1); ] in
   let up_or_down = match dir with | Up -> (line - 1, col) | Down -> (line + 1, col) in
   up_or_down :: sides
let neighbours dir line maxline col maxcol =
   List.filter
      (fun (line, col) -> 0 <= line && line <= maxline && 0 <= col && col <= maxcol)
      (neighbours dir line col)

let improve_cell dir labyrinth solution line col =
   let before_cost = solution.(line).(col) in
   let neighbours = neighbours dir line (Array.length labyrinth - 1) col (Array.length labyrinth.(0) - 1) in
   let neighbour_costs = List.map (fun (line, col) -> solution.(line).(col)) neighbours in
   let after_cost =
      match neighbour_costs with
      | [] -> assert false
      | c :: cs ->
         (List.fold_left min c cs)
         + cost_of_cell labyrinth.(line).(col)
   in
   let after_cost = min before_cost after_cost in
   solution.(line).(col) <- after_cost;
   after_cost < before_cost

let improve_line dir labyrinth solution line cols =
   List.fold_left
      (fun acc col -> improve_cell dir labyrinth solution line col || acc)
      false
      cols

let improve_line dir labyrinth solution line cols =
   let ltr = improve_line dir labyrinth solution line cols in
   let rtl = improve_line dir labyrinth solution line (List.rev cols) in
   ltr || rtl

let improve_labyrinth labyrinth solution =
   let max_line = Array.length labyrinth in
   let cols = List.init (Array.length labyrinth.(0)) Fun.id in
   let rec climb acc dir line =
      let line = match dir with Up -> line + 1 | Down -> line - 1 in
      if line >= max_line then
         acc
      else if line = 0 then
         let improves = improve_line dir labyrinth solution line cols in
         let acc = improves || acc in
         climb acc Up 0
      else
         let improves = improve_line dir labyrinth solution line cols in
         let acc = improves || acc in
         if improves then
            climb acc Down line
         else
            climb acc Up line
   in
   climb false Up (-1)

let solve labyrinth =
   let labyrinth = Labyrinth.to_array labyrinth in
   let solution = Array.map (Array.map (fun _ -> max_int - 2)) labyrinth in
   solution.(0) <- Array.map (function Wall -> max_int - 2 | Room -> 0) labyrinth.(0);
   let (_: bool) = improve_labyrinth labyrinth solution in
   assert (not (improve_labyrinth labyrinth solution));
   Array.to_list solution

