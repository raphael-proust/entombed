type t = int array list

(** [solve l] is a map of the cost (in number of necessary {e make-break}
    spells) to reach each cell.

    A make-break spell is a mechanic of the game which allows to switch the
    state of a cell: either {e making} or {e breaking} a wall.

    @return The dimensions of the solution match the dimension of the
    [labyrinth] parameter. *)
val solve : Labyrinth.t -> t
