(* current implementation based on Stdlib.Random *)
type t = Random.State.t
let init = function
   | Some i -> Random.State.make [| i |]
   | None -> Random.State.make_self_init ()
let cell prng = if Random.State.bool prng then Labyrinth.Wall else Labyrinth.Room
