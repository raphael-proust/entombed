(** The PRNG state, passed around to every function that needs it. *)
type t

(** [init] returns a new PRNG state. Pass [None] to self-initialise. *)
val init : int option -> t

(** [cell prng] is either [Wall] or [Room], pseudo randomly *)
val cell : t -> Labyrinth.cell
