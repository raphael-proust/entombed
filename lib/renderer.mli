(** Rendering mode:

    Whilst the core of the labyrinth generator handles short lines, the game
    would display a wider maze. Specifically, in the game each line is mirrored
    and padded. E.g., when the algorithm produces [██  █ ██], then the game's
    labyrinth contains the line

{[
████  █ ████ █  ████
^^                   two-wall padding
  ^^^^^^^^           original line
          ^^^^^^^^   mirrored line
                  ^^ two-wall padding
]}

*)

(** The display mode: it controls how the labyrinth is rendered on stdout.

    In all the constructors, the [wall] and [room] strings are used for
    representing the labyrinth's Walls and Rooms. These strings are intended to
    contain a single UTF-8 encoded code-point which displays as a single-width
    character. *)
type mode =
   | Classic of { wall: string; room: string; }
       (** Displays the labyrinth as per game visuals. *)
   | Raw of { wall: string; room: string; }
       (** Displays the lines as handled in memory (sans padding, sans
           mirroring). *)
   | SideBySide of { wall: string; room: string; }
       (** Displays the labyrinth on the left-hand side and the solution on the
           right-hand side. *)
   | InRoom of { wall: string; } (** Displays the cost in the rooms. *)

(***)

(** [parse ~wall ~room s] is the {!mode} value represented by [s] as specified
    below. If [s] does not conform to the format below, [parse] returns an
    [Error] with an error message.

    - ["classic:<wall>:<room>"] is for [Classic { wall; room; }].
    - ["raw:<wall>:<room>"] is for [Raw { wall; room; }].
    - ["side-by-side:<wall>:<room>"] is for [SideBySide { wall; room; }].
    - ["in-room:<wall>"] is for [InRoom { wall; }].

    The parameters [wall] and [room] are the default values to use in case the
    string [s] doesn't include the tileset suffix. For example,
    [parse ~wall ~room "classic"] is [Classic { wall; room }].

    You can substitute [:] in the strings above with any single-byte
    character, but you must use the same character for all the occurrences
    within the same string. E.g., ["raw+:+ "] is for
    [Raw { wall = ":"; room = " "].
    *)
val parse : wall:string -> room:string -> string -> (mode, string) result

(** [print] is a pretty-printer which conforms to the format described in the
    documentation of {!parse}.

    The [wall] and [room] parameter indicate the default tileset which is
    ommited from the printing if it matches those values. *)
val print: Format.formatter -> wall:string -> room:string -> mode -> unit

(** [render mode wall room l] displays the labyrinth [l] on the standard-output
    using the strings [wall] and [room] to represent the cells. The parameter
    [mode] sets the rendering mode.

    The strings for [wall] and [room] should be single-width unicode characters.
    Deviating from this may lead to display and alignment issues.

    @raise Some exception if the labyrinth and the solution are of different
    dimensions. *)
val render : mode -> Labyrinth.t -> Solution.t Lazy.t -> unit
