open Labyrinth
open Labyrinth.Syntax

type mode =
   | Classic of { wall: string; room: string; }
   | Raw of { wall: string; room: string; }
   | SideBySide of { wall: string; room: string; }
   | InRoom of { wall: string; }

(* mode parser and printer *)

let consume_prefix prefix s =
   let prefix_len = String.length prefix in
   let is_prefix prefix s =
      prefix_len <= String.length s
      &&
      let index = ref (-1) in
      String.for_all
         (fun c -> incr index; c = s.[!index])
         prefix
  in
  if is_prefix prefix s then
    Some (String.sub s prefix_len (String.length s - prefix_len))
  else
    None


(* TODO: better error management: distinguish string to-short from malformed *)
let ( let*? ) u f =
   match u with
   | `Uchar u -> f u
   | _ -> Error "Expected a unicode character"
let ( let*! ) c f =
   match c with
   | `Uchar u ->
      if not (Uchar.is_char u) then
        Error "Expected a single-byte character as separator"
      else
        f (Uchar.to_char u)
   | _ -> Error "Exepcted a character"
let string_of_uchar u =
   let b = Buffer.create 4 in
   Uutf.Buffer.add_utf_8 b u;
   Buffer.contents b

let read_tile ?expected_sep decoder =
   let*! sep = Uutf.decode decoder in
   match expected_sep with
   | Some exp when exp <> sep ->
       Error "Expected separator"
   | _ ->
     let*? tile = Uutf.decode decoder in
     let tile = string_of_uchar tile in
     Ok (sep, tile)

let parse ~wall ~room = function
   | "classic" -> Ok (Classic { wall; room; })
   | "raw" -> Ok (Raw { wall; room; })
   | "side-by-side" -> Ok (SideBySide { wall; room; })
   | "in-room" -> Ok (InRoom { wall; })
   | s ->
      let ( let* ) = Result.bind in
      match consume_prefix "classic" s with
      | Some suffix ->
          let decoder = Uutf.decoder (`String suffix) in
          let* (sep, wall) = read_tile decoder in
          let* (_, room) = read_tile ~expected_sep:sep decoder in
          Ok (Classic { wall; room; })
      | None ->
      match consume_prefix "raw" s with
      | Some suffix ->
          let decoder = Uutf.decoder (`String suffix) in
          let* (sep, wall) = read_tile decoder in
          let* (_, room) = read_tile ~expected_sep:sep decoder in
          Ok (Raw { wall; room; })
      | None ->
      match consume_prefix "side-by-side" s with
      | Some suffix ->
          let decoder = Uutf.decoder (`String suffix) in
          let* (sep, wall) = read_tile decoder in
          let* (_, room) = read_tile ~expected_sep:sep decoder in
          Ok (SideBySide { wall; room; })
      | None ->
      match consume_prefix "in-room" s with
      | Some suffix ->
          let decoder = Uutf.decoder (`String suffix) in
          let* (_, wall) = read_tile decoder in
          Ok (InRoom { wall; })
      | None ->
          Error "Unknown mode"


let print ppf ~wall ~room mode =
   (* TODO: detect when ':' is used in one of the wall/room and use a different sep *)
   let is_def w r = w = wall && r = room in
   let s =
    String.concat ":"
       begin
          match mode with
          | Classic { wall; room; } ->
              "classic" :: if is_def wall room then [] else [ wall; room ]
          | Raw { wall; room; } ->
              "raw" :: if is_def wall room then [] else [ wall; room ]
          | SideBySide { wall; room; } ->
              "side-by-side" :: if is_def wall room then [] else [wall; room ]
          | InRoom { wall=w; } ->
              "in-room" :: if w = wall then [] else [ w; ]
       end
   in
   Format.fprintf ppf "%s" s


(* actual rendering *)

let render_cell wall room = function
   | Wall -> print_string wall
   | Room -> print_string room

let render_line_classic wall room line =
   let line_length = length line in
   render_cell wall room Wall;
   render_cell wall room Wall;
   for i = 0 to line_length - 1 do
     render_cell wall room line.?(i)
   done;
   for i = line_length - 1 downto 0 do
     render_cell wall room line.?(i)
   done;
   render_cell wall room Wall;
   render_cell wall room Wall;
   print_newline ()

let render_line_raw wall room line =
   let line_length = length line in
   for i = 0 to line_length - 1 do
     render_cell wall room line.?(i)
   done;
   print_newline ()

let render_cost n =
   assert (0 <= n);
   if n <= 9 then
      (* single digit *)
      print_string (string_of_int n)
   else match n with
   | 10 -> print_char 'A'
   | 11 -> print_char 'B'
   | 12 -> print_char 'C'
   | 13 -> print_char 'D'
   | 14 -> print_char 'E'
   | 15 -> print_char 'F'
   | _ -> print_char '?'

let render_line_side_by_side wall room line solline =
   let line_length = length line in
   assert (line_length = Array.length solline);
   render_cell wall room Wall;
   render_cell wall room Wall;
   for i = 0 to line_length - 1 do
     render_cell wall room line.?(i)
   done;
   for i = line_length - 1 downto 0 do
     render_cost solline.(i)
   done;
   render_cost (solline.(0) + 1);
   render_cost (solline.(0) + 2);
   print_newline ()

let render_wall_or_cost wall cell sol =
   match cell with
   | Wall -> print_string wall
   | Room -> render_cost sol

let render_line_in_room wall line solline =
   let line_length = length line in
   assert (line_length = Array.length solline);
   render_wall_or_cost wall Wall (-1);
   render_wall_or_cost wall Wall (-1);
   for i = 0 to line_length - 1 do
     render_wall_or_cost wall line.?(i) solline.(i)
   done;
   for i = line_length - 1 downto 0 do
     render_wall_or_cost wall line.?(i) solline.(i)
   done;
   render_wall_or_cost wall Wall (-1);
   render_wall_or_cost wall Wall (-1);
   print_newline ()

let render mode t sol =
   match mode with
   | Classic { wall; room; } ->
       List.iter (render_line_classic wall room) (List.rev t)
   | Raw { wall; room; } ->
       List.iter (render_line_raw wall room) (List.rev t)
   | SideBySide { wall; room; } ->
       let sol = Lazy.force sol in
       List.iter2 (render_line_side_by_side wall room) (List.rev t) (List.rev sol)
   | InRoom { wall; } ->
       let sol = Lazy.force sol in
       List.iter2 (render_line_in_room wall) (List.rev t) (List.rev sol)
