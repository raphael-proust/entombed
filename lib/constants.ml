open Astring
open Cmdliner

open Labyrinth.Syntax

let display =
   let doc =
      "Set the rendering mode. The value must be one of 'classic' (classic \
       game display with mirroring and padding), 'raw' (simpler display \
       matching the internal working of the algorithm, 'side-by-side' \
       (displays the labyrinth along with the solution), or 'in-room' \
       (displays the walls and the solution in the rooms).\n\
       \n\
       You can specify the tileset for the displays with a suffix to these
       constants. For example, you can use 'classic:▓:░' to set the wall and
       room (respectively) tiles. The default for the tileset is ':▓:░'"
   in
   let wall = "▓" and room = "░" in
   let parse s = Result.map_error (fun msg -> `Msg msg) (Renderer.parse ~wall ~room s) in
   let print ppf m = Renderer.print ppf ~wall ~room m in
   let mode = Arg.conv ~docv:"DISPLAY" (parse, print) in
   let defmode = Renderer.Classic { wall; room } in
   Arg.(value & opt mode defmode & info ["display"] ~docv:"DISPLAY" ~doc)

let first_line =
   let first_line =
      let parse s =
         if String.length s < 3 then
            let msg = "unable to parse first-line: must be of length at least 3" in
            Error (`Msg msg)
         else if String.exists (fun c -> c <> '0' && c <> '1') s then
            let msg = "unable to parse first-line: use 0s and 1s only" in
            Error (`Msg msg)
         else
            let line =
               Array.init (String.length s) (fun i ->
                  match String.get s i with
                  | '0' -> Labyrinth.Room
                  | '1' -> Labyrinth.Wall
                  | _ -> assert false)
            in
            let line = Labyrinth.line line in
            Ok line
      in
      let print ppf line =
         for i = 0 to (Labyrinth.length line) - 1 do
            match line.?(i) with
            | Labyrinth.Room -> Format.fprintf ppf "%s" "0"
            | Labyrinth.Wall -> Format.fprintf ppf "%s" "1"
         done
      in
      Arg.conv ~docv:"LINE" (parse, print)
   in
   let default_first_line = Labyrinth.line [| Room; Room; Wall; Wall; Room; Room; Room; Wall |] in
   let doc =
      "Set the first (top) line of the labyrinth. You must describe \
       value as a sequence of at least three (3) zeros ('0') and ones ('1').
       E.g., 00110001. \
       \
       A '0' represents a Room, and a '1' represents a Wall."
   in
   Arg.(value & opt first_line default_first_line & info ["first-line"] ~docv:"LINE" ~doc)

let prng_seed =
   let doc = "The seed for the PRNG." in
   let none = "non-deterministic self initialisation" in
   Arg.(value & opt (some ~none int) None & info ["seed"] ~docv:"SEED" ~doc)

let number_of_lines =
   let number_of_lines =
      let parse s =
         match Arg.conv_parser Arg.int s with
         | Error _ as e -> e
         | Ok n when n <= 1 ->
               let msg = "unable to parse number-of-lines: use a value greater than 1" in
               Error (`Msg msg)
         | Ok n -> Ok n
      in
      let print = Arg.conv_printer Arg.int in
      Arg.conv ~docv:"N" (parse, print)
   in
   let doc = "The number of lines in the labyrinth. Must be more than one (1)." in
   Arg.(value & opt number_of_lines 17 & info ["n"; "number-of-lines"] ~docv:"N" ~doc)

let table =
   let default_table =
      (* see https://arxiv.org/ftp/arxiv/papers/1811/1811.02035.pdf *)
      (* [Some cell] when determined by the table, [None] when delegating to PRNG *)
      let open Labyrinth in
      [|
         Some Wall;
         Some Wall;
         Some Wall;
         None;
         Some Room;
         Some Room;
         None;
         None;
         Some Wall;
         Some Wall;
         Some Wall;
         Some Wall;
         None;
         Some Room;
         Some Room;
         Some Room;
         Some Wall;
         Some Wall;
         Some Wall;
         None;
         Some Room;
         Some Room;
         Some Room;
         Some Room;
         None;
         Some Room;
         Some Wall;
         None;
         None;
         Some Room;
         Some Room;
         Some Room;
      |]
   in
   let table =
      let parse s =
         if String.length s <> 32 then
            let msg = "unable to parse table: must be of length 32" in
            Error (`Msg msg)
         else if String.exists (fun c -> c <> '0' && c <> '1' && c <> '?') s then
            let msg = "unable to parse table: use 0s, 1s, and ?s only" in
            Error (`Msg msg)
         else
            let table =
               Array.init 32 (fun i ->
                  match String.get s i with
                  | '0' -> Some Labyrinth.Room
                  | '1' -> Some Labyrinth.Wall
                  | '?' -> None
                  | _ -> assert false)
            in
            Ok table
      in
      let print ppf table =
         Array.iter
            (function
               | Some Labyrinth.Room -> Format.fprintf ppf "%s" "0"
               | Some Labyrinth.Wall -> Format.fprintf ppf "%s" "1"
               | None -> Format.fprintf ppf "%s" "?")
            table
      in
      Arg.conv ~docv:"TABLE" (parse, print)
   in
   let doc =
         "Set the generator table for the labyrinth. You must describe the \
          generator table as a sequence of thirty-two (32) zeros ('0'), ones \
          ('1'), and question marks ('?'). A '0' represents \
          a room, a '1' represents a wall, a '?' represents a cell randomly \
          chosen during execution."
   in
   Arg.(value & opt table default_table & info ["table"] ~docv:"TABLE" ~doc)
