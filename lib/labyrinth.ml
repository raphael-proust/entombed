type cell =
   | Wall
   | Room
type line = cell array
let line a =
   if Array.length a < 3 then raise (Invalid_argument "Labyrinth.line: too small");
   Array.copy a
let length line = Array.length line
module Syntax = struct
   let ( .?() ) line i = line.(i)
end
type t = line list

let to_array labyrinth = Array.of_list labyrinth
