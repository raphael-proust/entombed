(** A [cell] is either a [Wall] or a [Room]. *)
type cell =
   | Wall
   | Room

(** A [line] is a read-only array of {!cell}s. *)
type line

(** [line a] is a line composed of the cells contained in the array [a]. *)
val line : cell array -> line

(** [length line] is the number of cells in [line].

    @raise [Invalid_argument] if [Array.length line < 3]
 *)
val length : line -> int

module Syntax : sig
   (** [line.?(i)] is the i-th cell of the line [line].

       @raise [Invalid_argument] if [i < 0] or [i >= length line]. *)
   val ( .?() ) : line -> int -> cell
end

(** A labyrinth ([t]) is a list of lines. *)
type t = line list

val to_array : t -> cell array array
