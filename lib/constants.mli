(** Game constants and other parameters

    The values in this module define the global constants for the generator
    (e.g., the size of the maze) and other aspects of the program. Each is
    command-line parameter with a default value matching the game's original
    value or the paper's default example. *)

(** Generation parameters *)

(** [first_line] is the first line of the labyrinth. *)
val first_line : Labyrinth.line Cmdliner.Term.t

(** [prng_seed] is the initialisation parameter for the PRNG. *)
val prng_seed : int option Cmdliner.Term.t

(** [number_of_line] is the number of line for the labyrinth. *)
val number_of_lines : int Cmdliner.Term.t

(** [table] determines the content of a cell based on the content of
    neighbourhing cells. See {!Generator.table} for details. *)
val table : Generator.table Cmdliner.Term.t

(** Rendering parameters *)

(** [mode] is the rendering mode. See {!Renderer.mode}. *)
val display : Renderer.mode Cmdliner.Term.t
