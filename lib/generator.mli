(** A generator [table] is used to decide the content of the next cell. A
    summary follows but full details should be sought from
    https://arxiv.org/ftp/arxiv/papers/1811/1811.02035.pdf.

    When generating the labyrinth, the content of a cell is determined by the
    content of some nearby cells (in a fashion similar to a cell automata).
    Specifically, it samples two cells on the same line to the left of the
    candidate and three cells from the previous line.

    The process samples five (5) neighbouring cells, which can be either Room or
    Wall, leading to 32 possible samples. For this reason, a [table] must have
    exactly 32 elements. The cells to the left are the most significant bits and
    the cells from the previous lines are the least significant bits.

    For each sample, the [table] contains a [cell option] interpreted as
    follows:
    - a [Some Wall] indicates that the candidate cell should contain a Wall,
    - a [Some Room] indicates that the candidate cell should contain a Room, and
    - a [None] indicates that the candidate cell should be chosen randomly. *)
type table = Labyrinth.cell option array

(** [generate ~prng table first_line number_of_lines] returns a labyrinth (see
    {!Labyrinth.t}). The labyrinth has [number_of_lines] lines, the first one
    being [first_line].

    @raise [Invalid_argument] if [Array.length table <> 32]

    @raise [Invalid_argument] if [number_of_lines < 1] *)
val generate : prng:Prng.t -> table -> Labyrinth.line -> int -> Labyrinth.t
