let table =
   let wall = Some true and room = Some false in
   [| wall; wall; wall; None; room; room; None; None;
      wall; wall; wall; wall; None; room; room; room;
      wall; wall; wall; None; room; room; room; room;
      None; room; wall; None; None; room; room; room; |]

let cell = Random.self_init (); fun () -> Random.bool ()
let cell5 a b c d e =
   let ( << ) b i = (if b then 1 else 0) lsl i in
   try Option.get @@ table.((e << 0) lxor (d << 1) lxor (c << 2) lxor (b << 3) lxor (a << 4))
   with Invalid_argument _ -> cell ()

let l = Array.init 17 (fun _ -> Array.make 8 false)
let () = l.(0) <- [| false; false; true; true; false; false; false; true |]
let () =
   for y = 1 to 16 do for x = 0 to 7 do
      l.(y).(x) <- (
         if x = 0 then
            cell5 true false (cell ()) l.(y-1).(x) l.(y-1).(x+1)
         else if x = 1 then
            cell5 false l.(y).(x-1) l.(y-1).(x-1) l.(y-1).(x) l.(y-1).(x+1)
         else if x < 7 then
            cell5 l.(y).(x-2) l.(y).(x-1) l.(y-1).(x-1) l.(y-1).(x) l.(y-1).(x+1)
         else (* x = 7 *)
            cell5 l.(y).(x-2) l.(y).(x-1) l.(y-1).(x-1) l.(y-1).(x) (cell ()))
   done done

let () =
   let render wall = print_string (if wall then "▓" else "░") in
   for y = 0 to 16 do
      render true; render true;
      for x = 0 to 7 do render l.(y).(x) done;
      for x = 7 downto 0 do render l.(y).(x) done;
      render true; render true;
      print_newline ()
   done
