open Cmdliner
open Entombed

let runs =
   let doc = "The number of experiments to run." in
   Arg.(value & opt int 1 & info ["r"; "runs"] ~docv:"RUNS" ~doc)

let resdisplay =
   let doc = "Set the display style for the results." in
   let display = Arg.enum [ ("all", `All); ("num", `Num); ("bar", `Bar); ("tsv", `Tsv); ("csv", `Csv); ] in
   let defdisplay = `All in
   Arg.(value & opt display defdisplay & info ["display"] ~docv:"DISPLAY" ~doc)


module Results = struct
   module IntMap = Map.Make(Int)
   let init = IntMap.empty
   let bump r n = IntMap.update n (function None -> Some 1 | Some n -> Some (succ n)) r
   let get r =
      let (high, _) = IntMap.max_binding r in
      List.init
        (high + 1)
        (fun i -> Option.value ~default:0 @@ IntMap.find_opt i r)
   let pp_xsv s ppf r =
      let r = get r in
      List.iteri (fun i v -> Format.fprintf ppf "%d%s%d\n" i s v) r
   let pp_numerical ppf r =
      let r = get r in
      let width = String.length (string_of_int (List.length r + 1)) in
      List.iteri (Format.fprintf ppf "%*d: %d\n" width) r
   let repeat_string chunk n =
      let chunk_size = String.length chunk in
      let b = Bytes.create (chunk_size * n) in
      for i = 0 to (n - 1) do
         Bytes.blit_string chunk 0 b (i * chunk_size) chunk_size
      done;
      Bytes.unsafe_to_string b
   let pp_bars ppf r =
      let r = get r in
      let total = List.fold_left (+) 0 r in
      let width = String.length (string_of_int (List.length r - 1)) in
      List.iteri
         (fun i n ->
            let pct = (100. *. float_of_int n) /. float_of_int total in
            let bar_len = int_of_float pct in
            Format.fprintf ppf "%*d: %s\n" width i (repeat_string "▓" bar_len))
         r
   let pp_all ppf r =
      let r = get r in
      let total = List.fold_left (+) 0 r in
      let width = String.length (string_of_int (List.length r - 1)) in
      List.iteri
         (fun i n ->
            let pct = (100. *. float_of_int n) /. float_of_int total in
            let bar_len = int_of_float pct in
            Format.fprintf ppf "%*d: %s%s %.2f (%d)\n" width
               i
               (repeat_string "▓" bar_len)
               (repeat_string "░" (100 - bar_len))
               pct
            n)
         r
   let display results = function
      | `All -> Format.printf "%a%!" pp_all results
      | `Num -> Format.printf "%a%!" pp_numerical results
      | `Bar -> Format.printf "%a%!" pp_bars results
      | `Tsv -> Format.printf "%a%!" (pp_xsv "\t") results
      | `Csv -> Format.printf "%a%!" (pp_xsv ",") results
end

let solution runs prng_seed table first_line number_of_lines display =
   let prng = Prng.init prng_seed in
   let rec run n results =
      if n <= 0 then
         results
      else
         let labyrinth = Generator.generate ~prng table first_line number_of_lines in
         let solution = Solution.solve labyrinth in
         let overall = Array.fold_left min max_int (List.hd (List.rev solution)) in
         let results = Results.bump results overall in
         run (n - 1) results
   in
   let results = run runs Results.init in
   Results.display results display;
   Cmdliner.Cmd.Exit.ok

let cmd =
   let open Constants in
   let doc = "generate a random labyrinth" in
   Cmd.v
     (Cmd.info "entombed" ~doc)
     (Term.(const solution $ runs $ prng_seed $ table $ first_line $ number_of_lines $ resdisplay))

let () = Stdlib.exit @@ Cmd.eval' cmd
