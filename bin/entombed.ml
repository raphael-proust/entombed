open Cmdliner
open Entombed

let labyrinth prng_seed table first_line number_of_lines display =
   let prng = Prng.init prng_seed in
   let labyrinth = Generator.generate ~prng table first_line number_of_lines in
   let solution = lazy (Solution.solve labyrinth) in
   Renderer.render display labyrinth solution;
   Cmd.Exit.ok

let cmd =
   let open Constants in
   let doc = "generate a random labyrinth" in
   Cmd.v
     (Cmd.info "entombed" ~doc)
     Term.(const labyrinth $ prng_seed $ table $ first_line $ number_of_lines $ display)

let () = Stdlib.exit @@ Cmd.eval' cmd
