# Entombed-like labyrinth generator

See https://en.wikipedia.org/wiki/Entombed_(Atari_2600)

## Usage

Install with opam:

```
opam install .
```

This installs the `entombed` binary:

```
$ entombed
▓▓░░▓▓░░░▓▓░░░▓▓░░▓▓
▓▓▓▓▓░░▓▓▓▓▓▓░░▓▓▓▓▓
▓▓░░▓░▓▓░░░░▓▓░▓░░▓▓
▓▓▓▓▓░░▓▓░░▓▓░░▓▓▓▓▓
▓▓░▓░░▓▓░░░░▓▓░░▓░▓▓
▓▓▓▓░▓▓░░▓▓░░▓▓░▓▓▓▓
▓▓▓░░░▓▓░▓▓░▓▓░░░▓▓▓
▓▓░░▓▓▓░░░░░░▓▓▓░░▓▓
▓▓▓▓▓░░░▓▓▓▓░░░▓▓▓▓▓
▓▓▓░░░▓▓░░░░▓▓░░░▓▓▓
▓▓▓▓▓░▓░░▓▓░░▓░▓▓▓▓▓
▓▓░░░░▓▓░░░░▓▓░░░░▓▓
▓▓░▓▓░▓░░▓▓░░▓░▓▓░▓▓
▓▓░▓░░▓░▓▓▓▓░▓░░▓░▓▓
▓▓▓▓▓░▓░░░░░░▓░▓▓▓▓▓
▓▓░░▓░▓░▓▓▓▓░▓░▓░░▓▓
▓▓░▓▓░▓░░▓▓░░▓░▓▓░▓▓
```

The `entombed` binary prints a labyrinth on its standard output. It can be
tweaked via numerous command-line parameters.

```
NAME
       entombed - generate a random labyrinth

SYNOPSIS
       entombed [OPTION]...

OPTIONS
       --display=DISPLAY (absent=classic)
           Set the rendering mode. The value must be one of 'classic'
           (classic game display with mirroring and padding), 'raw' (simpler
           display matching the internal working of the algorithm,
           'side-by-side' (displays the labyrinth along with the solution),
           or 'in-room' (displays the walls and the solution in the rooms).
           You can specify the tileset for the displays with a suffix to
           these constants. For example, you can use 'classic:▓:░' to set
           the wall and room (respectively) tiles. The default for the
           tileset is ':▓:░'

       --first-line=LINE (absent=00110001)
           Set the first (top) line of the labyrinth. You must describe value
           as a sequence of at least three (3) zeros ('0') and ones ('1').
           E.g., 00110001. A '0' represents a Room, and a '1' represents a
           Wall.

       --help[=FMT] (default=auto)
           Show this help in format FMT. The value FMT must be one of `auto',
           `pager', `groff' or `plain'. With `auto', the format is `pager` or
           `plain' whenever the TERM env var is `dumb' or undefined.

       -n N, --number-of-lines=N (absent=17)
           The number of lines in the labyrinth. Must be more than one (1).

       --seed=SEED (absent=non-deterministic self initialisation)
           The seed for the PRNG.

       --table=TABLE (absent=111?00??1111?000111?0000?01??000)
           Set the generator table for the labyrinth. You must describe the
           generator table as a sequence of thirty-two (32) zeros ('0'), ones
           ('1'), and question marks ('?'). A '0' represents a room, a '1'
           represents a wall, a '?' represents a cell randomly chosen during
           execution.
```

## Sources

The `entombed` sources are help in the `src/` directory. The different
components of the program are separated into distinct compilation units:

- `constant.ml`: constants (e.g., height of the labyrinth) and argument parsing
  to set those constants via the command line (e.g., `--number-of-lines`).
- `entombed.ml`: main entry-point of the program.
- `generator.ml`: labyrinth generator.
- `labyrinth.ml`: type definitions for the internal representation of a
  labyrinth as well as some primitives to handle values of those types.
- `prng.ml`: pseudo-random number generator.
- `renderer.ml`: rendering of a labyrinth.

The `entombed` sources are written in a somewhat over-engineered way with ample
documentation.

The main purpose of this project is didactic and educational.

## Sources (more)

The `entombed.ml` file at the top-level directory of this project is a compact,
single-file, no-dependency version of `entombed`. It ignores all command-line
parameters.

```
$ ocaml entombed.ml
▓▓░░▓▓░░░▓▓░░░▓▓░░▓▓
▓▓▓▓░░░▓▓▓▓▓▓░░░▓▓▓▓
▓▓▓░░▓▓░░▓▓░░▓▓░░▓▓▓
▓▓▓░▓▓░░▓▓▓▓░░▓▓░▓▓▓
▓▓▓░▓░░▓▓░░▓▓░░▓░▓▓▓
▓▓▓░▓▓░▓░░░░▓░▓▓░▓▓▓
▓▓▓░░░░▓░▓▓░▓░░░░▓▓▓
▓▓░░▓▓░▓░▓▓░▓░▓▓░░▓▓
▓▓░▓▓░░▓░▓▓░▓░░▓▓░▓▓
▓▓▓▓░░▓▓░▓▓░▓▓░░▓▓▓▓
▓▓░░░▓▓░░░░░░▓▓░░░▓▓
▓▓▓▓░▓░░▓▓▓▓░░▓░▓▓▓▓
▓▓░▓░▓▓░░▓▓░░▓▓░▓░▓▓
▓▓░▓░▓░░▓▓▓▓░░▓░▓░▓▓
▓▓▓▓░▓░▓▓░░▓▓░▓░▓▓▓▓
▓▓▓░░▓░░░░░░░░▓░░▓▓▓
▓▓░░▓▓░▓▓▓▓▓▓░▓▓░░▓▓
```
